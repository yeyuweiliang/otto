﻿$(document).ready(function(){
	$("#nav").find("li").not($("li.on")).hover(function() {
		$(this).toggleClass("on");
	});
	$("#nav").find("li").each(function(){
		$(this).hover(function(){
			$(this).find("#small").slideDown(200);
		},function(){
			$(this).find("#small").slideUp(100);
		});
	});
	
	$("#photos").find("li").hover(function() {
		$(this).find("a").animate({top: "0"}, 350);
	},function(){
		$(this).find("a").animate({top: "180"}, 350);
	});
	
	$("#indexschool #sleft .schoolimg").eq(0).show();
    $("#schoolname div").eq(0).addClass("on");
    $("#schoolname div").click(function(){
    var num =$("#schoolname div").index(this);
    $("#schoolname div").removeClass("on");
    $(this).addClass("on");
    $("#indexschool #sleft .schoolimg").hide();
    $("#indexschool #sleft .schoolimg").eq(num).show().slblings().hide();
	});



	
	 $("#indexschool #sleft .schoolimg .d1 .scroll").scrollable({size:1,items:"#indexschool #sleft .schoolimg .d1 .scroll",speed:500,loop:true}).autoscroll({autoplay:false,interval:5000,steps:1}).navigator({navi:".home_01_u1",naviItem:"li",activeClass:"focus"}); 
	
	
	var swiper2 = new Swiper('.swiper2', {
	loop : true,
	pagination: '.page2',
	effect: 'coverflow',
	paginationClickable: true,
	grabCursor: true,
	centeredSlides: true,
	nextButton: '.swiper-button-next',
	prevButton: '.swiper-button-prev',
	slidesPerView: 'auto',
	autoplay: 5000,
	autoplayDisableOnInteraction: false,
	coverflow: {
		rotate: 0,// 旋转的角度
		stretch: 50,// 拉伸   图片间左右的间距和密集度
		depth: 100,// 深度   切换图片间上下的间距和密集度
		modifier: 3,// 修正值 该值越大前面的效果越明显
		slideShadows : false// 页面阴影效果
	}
});
	
	
	
});
function sethome(obj,url){ 
	try{ 
		obj.style.behavior='url(#default#homepage)';obj.setHomePage(url); 
	} 
	catch(e){ 
		if(window.netscape) { 
			try { 
				netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect"); 
			} 
			catch (e) { 
				alert("此操作被浏览器拒绝！\n请在浏览器地址栏输入“about:config”并回车\n然后将 [signed.applets.codebase_principal_support]的值设置为'true',双击即可。"); 
			} 
			var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch); 
			prefs.setCharPref('browser.startup.homepage',url); 
		}else{ 
			alert("您的浏览器不支持，请按照下面步骤操作：\n1、打开浏览器设置。\n2、点击设置网页。\n3、输入："+url+"点击确定。"); 
		} 
	} 
}
function shoucang(sTitle,sURL) { 
	try{ 
		window.external.addFavorite(sURL, sTitle); 
	} 
	catch (e){ 
		try{ 
			window.sidebar.addPanel(sTitle, sURL, ""); 
		} 
		catch (e){ 
			alert("加入收藏失败，请使用Ctrl+D进行添加"); 
		} 
	} 
}
function changebaike(ii,allnum){
	for(var i=1;i<=allnum;i++){
		if(i==ii){
			$("#bkitem_" + ii).addClass("on");
			$(".blist_" + ii).show();
			$(".more_" + ii).show();
		}else{
			$("#bkitem_" + i).removeClass("on");
			$(".blist_" + i).hide();
			$(".more_" + i).hide();
		}
	}
}
function change_write(ii,allnum){
	for(var i=1;i<=allnum;i++){
		if(i==ii){
			$("#bt_" + ii).addClass("on");
			$(".sel_" + ii).show();
		}else{
			$("#bt_" + i).removeClass("on");
			$(".sel_" + i).hide();
		}
	}
}
function checklisten(){
	var formname = document.formlisten;
	if(formname.username.value==""){alert("请输入姓名！");formname.username.focus();return false;}
	if(formname.tel.value==""||formname.tel.value.length!=11){alert("请输入正确的手机号码！");formname.tel.focus();return false;}
	if(formname.age.value==""){alert("请输入小孩年龄！");formname.age.focus();return false;}
	if(formname.school.value==""){alert("请选择校区！");formname.school.focus();return false;}
	else
	return true;
}
function checktest(){
	var formname = document.formtest;
	if(formname.username.value==""){alert("请输入姓名！");formname.username.focus();return false;}
	if(formname.tel.value==""||formname.tel.value.length!=11){alert("请输入正确的手机号码！");formname.tel.focus();return false;}
	if(formname.age.value==""){alert("请输入小孩年龄！");formname.age.focus();return false;}
	if(formname.school.value==""){alert("请选择校区！");formname.school.focus();return false;}
	else
	return true;
}
function checkjoin(){
	var formname = document.formjoin;
	if(formname.truename.value==""){alert("请输入您的姓名！");formname.truename.focus();return false;}
	if(formname.tel.value==""||formname.tel.value.length!=11){alert("请输入正确的手机号码！");formname.tel.focus();return false;}
	if(formname.city.value==""){alert("请输入加盟省市！");formname.city.focus();return false;}
	if(formname.jingyan.value==""){alert("请输入行业经验！");formname.jingyan.focus();return false;}
	else
	return true;
}
function showschool(ii,allnum,id){
	$("#schoolimg").html("<center><img src='images/loading.gif' style='margin-top:180px;' /></center>");
	for(i=1;i<=allnum;i++){
		if(i==ii){
			$("#school" + ii).addClass("on");
			$.ajax({
			   type: "POST",
			   url: "js/showschool.php",
			   data: "id=" + id,
			   success: function(msg){
				 if(msg=="error"){
					 return false;
				 }else{
					 $("#schoolimg").html(msg);
				 }
			   }
			});
		}else{
			$("#school" + i).removeClass("on");
		}
	}
}





















