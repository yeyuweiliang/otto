<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:78:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\index\index.html";i:1534562329;s:80:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\header.html";i:1534562487;s:83:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\left_menu.html";i:1534756619;s:81:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\base_js.html";i:1533819104;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        Otto2美术教育
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="__STATIC__/admin/css/x-admin.css" media="all">
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
          UE.getEditor('content',{    //content为要编辑的textarea的id
          initialFrameWidth: 1100,   //初始化宽度
          initialFrameHeight: 500,   //初始化高度
  });
</script>
</head>
    <body>
        <div class="layui-layout layui-layout-admin">
            <div class="layui-header header header-demo">
                <div class="layui-main">
                    <a class="logo" href="<?php echo url('index/index'); ?>">
                        Otto2美术教育
                    </a>
                    <ul class="layui-nav" lay-filter="">
                      <li class="layui-nav-item"><img src="__STATIC__/admin/images/logo.png" class="layui-circle" style="border: 2px solid #A9B7B7;" width="35px" alt=""></li>
                      <li class="layui-nav-item">
                        <a href="javascript:;">admin</a>
                        <dl class="layui-nav-child"> <!-- 二级菜单 -->

                          <dd><a href="<?php echo url('login/logout'); ?>">退出</a></dd>
                        </dl>
                      </li>

                      <li class="layui-nav-item x-index"><a href="/">前台首页</a></li>
                    </ul>
                </div>
            </div>


            <div class="layui-side layui-bg-black x-side">
    <div class="layui-side-scroll">
        <ul class="layui-nav layui-nav-tree site-demo-nav" lay-filter="side">
            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe634;</i><cite>轮播管理</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('banner/index'); ?>">
                            <cite>轮播列表</cite>
                        </a>
                    </dd>
                    </dd>
                </dl>
            </li>

           <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe604;</i><cite>网站头部</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('header/index'); ?>">
                            <cite>关键词，描述</cite>
                        </a>
                    </dd>
                </dl>
            </li>

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe60a;</i><cite>首页管理</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('bannersy/index'); ?>">
                            <cite>首页轮播图</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('wonderful/index'); ?>">
                            <cite>精彩课程</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('works/index'); ?>">
                            <cite>优秀作品</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('teacher/index'); ?>">
                            <cite>教师风采</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('huiguang/index'); ?>">
                            <cite>会馆分布</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('yuyue/index'); ?>">
                            <cite>预约免费报名</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('huodong/index'); ?>">
                            <cite>特色活动</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('kecshow/index'); ?>">
                            <cite>课程展示</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('honor/index'); ?>">
                            <cite>企业荣誉</cite>
                        </a>
                    </dd>
                </dl>
            </li>

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe638;</i><cite>品牌介绍</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('pinpai/index'); ?>">
                            <cite>介绍详情</cite>
                        </a>
                    </dd>
                </dl>
            </li>
            
            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe650;</i><cite>精彩课程</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('courses/index'); ?>">
                            <cite>课程设置</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('category/index'); ?>">
                            <cite>课程展示</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('cate/index'); ?>">
                            <cite>课程详情</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('teskec/index'); ?>">
                            <cite>课程特色</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('teskecimg/index'); ?>">
                            <cite>课程特色右边图片</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('team/index'); ?>">
                            <cite>教研团队</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('jiaoyu/index'); ?>">
                            <cite>教育成果</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('anlicz/index'); ?>">
                            <cite>儿童成长案例</cite>
                        </a>
                    </dd>
                </dl>
            </li>
            
            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe62e;</i><cite>学员成长</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('students/index'); ?>">
                            <cite>学员案例</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('studentsxq/index'); ?>">
                            <cite>学员详情</cite>
                        </a>
                    </dd>
                </dl>
            </li>

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe63a;</i><cite>新闻中心</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('news/index'); ?>">
                            <cite>新闻列表</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('newsxq/index'); ?>">
                            <cite>新闻详情</cite>
                        </a>
                    </dd>
                </dl>
            </li>

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe636;</i><cite>师资实力</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('teachers/index'); ?>">
                            <cite>培训教师</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('teachersxq/index'); ?>">
                            <cite>教师详情</cite>
                        </a>
                    </dd>
                </dl>
            </li>      

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe62a;</i><cite>课程试听</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('coursest/index'); ?>">
                            <cite>试听</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('tuijian/index'); ?>">
                            <cite>推荐课程</cite>
                        </a>
                    </dd>
                </dl>
            </li>      
            
            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe643;</i><cite>人才招聘</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('Rencai/index'); ?>">
                            <cite>招聘列表</cite>
                        </a>
                    </dd>
                </dl>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('Rencaixq/index'); ?>">
                            <cite>招聘详情</cite>
                        </a>
                    </dd>
                </dl>
            </li>

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe63a;</i><cite>联系我们</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('contact/index'); ?>">
                            <cite>联系我们详情</cite>
                        </a>
                    </dd>
                </dl>
            </li>

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe613;</i><cite>管理员管理</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('admin/index'); ?>">
                            <cite>管理员列表</cite>
                        </a>
                    </dd>

                </dl>
            </li>
            
            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe611;</i><cite>留言管理</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('message/index'); ?>">
                            <cite>留言列表</cite>
                        </a>
                    </dd>

                </dl>
            </li>

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe637;</i><cite>友情链接</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('link/index'); ?>">
                            <cite>友情管理</cite>
                        </a>
                    </dd>

                </dl>
            </li>

            <li class="layui-nav-item">
                <a class="javascript:;" href="javascript:;">
                    <i class="layui-icon" style="top: 3px;">&#xe614;</i><cite>系统设置</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd class="">
                        <a href="javascript:;" _href="<?php echo url('system/index'); ?>">
                            <cite>系统设置</cite>
                        </a>
                    </dd>

                </dl>
            </li>
            <li class="layui-nav-item" style="height: 30px; text-align: center">
            </li>
        </ul>
    </div>

</div>

            <div class="layui-tab layui-tab-card site-demo-title x-main" lay-filter="x-tab" lay-allowclose="true">
                <div class="x-slide_left"></div>
                <ul class="layui-tab-title">
                    <li class="layui-this">
                        我的桌面
                        <i class="layui-icon layui-unselect layui-tab-close">ဆ</i>
                    </li>
                </ul>
                <div class="layui-tab-content site-demo site-demo-body">
                    <div class="layui-tab-item layui-show">
                        <iframe frameborder="0" src="<?php echo url('index/welcome'); ?>" class="x-iframe"></iframe>
                    </div>
                </div>
            </div>
            <div class="site-mobile-shade">
            </div>
        </div>
        <script src="__STATIC__/admin/lib/layui/layui.js" charset="utf-8"></script>
<script src="__STATIC__/admin/js/x-admin.js"></script>
<script src="__STATIC__/admin/js/jquery.min.js"></script>
<script src="__STATIC__/admin/js/x-layui.js"></script>
<!--引入boostrap-->
<link rel="stylesheet" type="text/css" href="__STATIC__/admin/lib/bootstrap/css/bootstrap.css" />
<script type="text/javascript" src="__STATIC__/admin/lib/bootstrap/js/bootstrap.js"></script>

    </body>
</html>