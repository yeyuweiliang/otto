<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:83:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\admin\admin_edit.html";i:1534832596;s:80:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\header.html";i:1534562487;s:81:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\base_js.html";i:1533819104;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        Otto2美术教育
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="__STATIC__/admin/css/x-admin.css" media="all">
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
          UE.getEditor('content',{    //content为要编辑的textarea的id
          initialFrameWidth: 1100,   //初始化宽度
          initialFrameHeight: 500,   //初始化高度
  });
</script>
</head>
    
    <body>
        <div class="x-body">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="username" class="layui-form-label">
                        <span class="x-red">*</span>登录名
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="username" name="username" required="" lay-verify="required" value="<?php echo $admin['username']; ?>"
                        autocomplete="off" class="layui-input"disabled>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span>用户名不得修改
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="L_email" class="layui-form-label">
                        <span class="x-red">*</span>邮箱
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="L_email" name="email" required="" lay-verify="email" value="<?php echo $admin['email']; ?>"
                        autocomplete="off" class="layui-input">
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="L_pass" class="layui-form-label">
                        <span class="x-red">*</span>新密码
                    </label>
                    <div class="layui-input-inline">
                        <input type="password" id="L_pass" name="password"  class="layui-input" value="" placeholder="密码">
                    </div>

                </div>

                <!--添加隐藏字段-->
                <input type="hidden" name="id" value="<?php echo \think\Session::get('user_info.id'); ?>">
                <input type="hidden" name="is_update" value="<?php echo \think\Session::get('user_info.is_update'); ?>">


                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label">
                    </label>
                    <button  class="layui-btn" lay-filter="save" lay-submit="" id="submit">
                        保存
                    </button>
                </div>
            </form>
        </div>
       <script src="__STATIC__/admin/lib/layui/layui.js" charset="utf-8"></script>
<script src="__STATIC__/admin/js/x-admin.js"></script>
<script src="__STATIC__/admin/js/jquery.min.js"></script>
<script src="__STATIC__/admin/js/x-layui.js"></script>
<!--引入boostrap-->
<link rel="stylesheet" type="text/css" href="__STATIC__/admin/lib/bootstrap/css/bootstrap.css" />
<script type="text/javascript" src="__STATIC__/admin/lib/bootstrap/js/bootstrap.js"></script>
        <script>
            layui.use(['form','layer'], function(){
                $ = layui.jquery;
              var form = layui.form()
              ,layer = layui.layer;
            


              
              
            });
        </script>

        <script>
            $(function(){
                $("#submit").on('click',function(){
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo url('admin/update'); ?>",
                        data: $(".layui-form").serialize(),
                        dataType: "json",
                        success: function(data){
                            console.log(data);
                            if (data.status == 1) {
                                alert(data.message);
                                window.location.href = "<?php echo url('admin/index'); ?>";
                            } else {
                                alert(data.message);
                                window.location.href = "<?php echo url('admin/ediit'); ?>";
                            }
                        }
                    })
                })
            })
        </script>

    </body>

</html>