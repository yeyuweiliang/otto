<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:86:"D:\PHPTutorial\WWW\pen\otto2\public/../application/index\view\students\studentslb.html";i:1534747132;s:80:"D:\PHPTutorial\WWW\pen\otto2\public/../application/index\view\common\header.html";i:1534520333;s:84:"D:\PHPTutorial\WWW\pen\otto2\public/../application/index\view\common\rightitems.html";i:1534524128;s:80:"D:\PHPTutorial\WWW\pen\otto2\public/../application/index\view\common\footer.html";i:1534562816;}*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $name['name']; ?>  学员成长 - 武汉OttO2教育官网</title>
<meta name="keywords" content="<?php echo $header['3']['keywords']; ?>" />
<meta name="description" content="<?php echo $header['3']['description']; ?>" />
<link rel="shortcut icon" href="__STATIC__/index/images/favicon.ico">
<link href="__STATIC__/index/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="__STATIC__/index/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="__STATIC__/index/js/commen.js"></script>
</head>
<body>

<!--头部开始-->
<?php $on=4; ?>
<!--顶部到导航栏-->
<div id="toparea">
	<div id="tarea">
		<div id="lword">Otto2艺术美学</div>
		<div id="rlink">
			<a href="javascript:void(0);" onclick="javascript:shoucang(document.title,document.URL);">加入收藏</a>&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="javascript:void(0);" onclick="javascript:sethome(this,document.URL)">设为首页</a>
		</div>
	</div>
</div>

<div id="top">
	<h1 id="logo"><a href="index.htm" title="优佳加-儿童感统训练"></a></h1>
	<div id="tw"></div>
	<div id="erwema"><img src="__STATIC__/index/images/erweima.gif"  /><br>扫一扫开启智能教学体验</div>
	<div id="tright">
		<div id="telname">统一服务热线</div>
		<div id="toptel">400-920-3588</div>
	</div>
</div>

<div id="topmenu">
	<div id="nav">
		<ul>
			<li <?php if(isset($on)&&($on==1)){?> class="on" <?php }?>>
				<a title="首页" href="<?php echo url('index/index'); ?>">首&nbsp;&nbsp;&nbsp;&nbsp;页</a>
			</li>
			<li <?php if(isset($on)&&($on==2)){?> class="on" <?php }?>>
				<a title="品牌介绍" href="<?php echo url('pinpai/index'); ?>">品牌介绍</a>
			</li>
			<li <?php if(isset($on)&&($on==3)){?> class="on" <?php }?>>
				<a title="精彩课程" href="<?php echo url('jingckec/index'); ?>">精彩课程</a>
			</li>
			<li <?php if(isset($on)&&($on==4)){?> class="on" <?php }?>>
				<a title="学员成长" href="<?php echo url('students/index'); ?>">学员成长</a>
			</li>
			<li <?php if(isset($on)&&($on==5)){?> class="on" <?php }?>>
				<a title="新闻中心" href="<?php echo url('news/index'); ?>">新闻中心</a>
			</li>
			<li <?php if(isset($on)&&($on==6)){?> class="on" <?php }?>>
				<a title="师资实力" href="<?php echo url('teachers/index'); ?>">师资实力</a>
			</li>
			<li <?php if(isset($on)&&($on==7)){?> class="on" <?php }?>>
				<a title="机构简介" href="<?php echo url('Coursest/index'); ?>" >课程试听</a>
			</li>
			<li <?php if(isset($on)&&($on==8)){?> class="on" <?php }?>>
				<a title="人才招聘" href="<?php echo url('Rencai/index'); ?>">人才招聘</a>
			</li>
			<li <?php if(isset($on)&&($on==9)){?> class="on" <?php }?>>
				<a title="联系我们" href="<?php echo url('contact/index'); ?>" >联系我们</a>
			</li>
		</ul>
	</div>
</div>
<!--顶部到导航栏-->
 <!--头部结束-->

<div id="position">
	<div id="pagename">儿童成长案例</div>
    <div id="pagepos">您的位置：<a href="<?php echo url('index/index'); ?>">首页</a> > <a href="<?php echo url('students/index'); ?>">学员成长</a> > <?php echo $name['name']; ?></div>
</div>
<div id="pagemain">
	<div id="leftinfo">
    	<div id="sttop">
        	<div id="limg"><img src="__ROOT__/uploads/<?php echo $detail['image']; ?>" width="420" height="270" /></div>
            <div id="rdata">
            	<div id="name"><?php echo $detail['name']; ?></div>
                <div id="course"><?php echo $detail['course']; ?></div>
                <div id="notes">
                	孩子年龄：<?php echo $detail['age']; ?>岁<br />
                    所报课程：<?php echo $detail['course']; ?><br />
                    训练成果：<?php echo $detail['chengg']; ?><br />
                </div>
            </div>
        </div>
        <div id="theinfo">
        	<div id="title">训练前</div>
            <div id="contents">
            	<?php echo $detail['xunlq']; ?>            </div>
        </div>
        <div id="theinfo">
        	<div id="title">训练后</div>
            <div id="contents">
            	<?php echo $detail['xunlh']; ?>            </div>
        </div>
        <div id="pingjia">
        	<div id="row"></div>
            <div id="words">
            	<div id="title"><div id="name">家长评价</div><div id="theline"></div></div>
                <div id="pj">
                	<?php echo $detail['pj']; ?>                </div>
            </div>
        </div>
        <div id="otherlink">
                上一位学员：
                <?php if($prev == ''): ?>
                <a href="javascript:void(0);">没有啦
                </a>
                <?php else: ?>
                <a href="<?php echo url('students/detail',array('id'=>$prev)); ?>" title='<?php echo $name_prev['name']; ?>'><?php echo $name_prev['name']; ?>
                </a>
                <?php endif; ?>
                <br />
        下一位学员：
                <?php if($next == ''): ?>
                <a href="javascript:void(0);">没有啦
                </a>
                <?php else: ?>
        <a href="<?php echo url('students/detail',array('id'=>$next)); ?>" title='<?php echo $name_next['name']; ?>'><?php echo $name_next['name']; ?>
        </a>       
        <?php endif; ?>        
    </div>
    </div>

<!-- 教师学院右边共用 -->
    
<!-- 教师学院右边共用 -->
    <div id="rightitems">
        <div id="reccourse">
	<div id="title">相关课程推荐<a href="<?php echo url('Coursest/index'); ?>">more</a></div>
    <div id="imgs">
                <?php if(is_array($tuijian) || $tuijian instanceof \think\Collection || $tuijian instanceof \think\Paginator): $i = 0; $__LIST__ = $tuijian;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
    	    	<a href="<?php echo url('Coursest/index'); ?>"><img src="__ROOT__/uploads/<?php echo $vo['image']; ?>" width="280" height="180" /></a>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
</div>        <div id="recteacher">
	<div id="title">学员成长案例<a href="<?php echo url('students/index'); ?>">more</a></div>
    <div id="imgs">
           <?php if(is_array($right) || $right instanceof \think\Collection || $right instanceof \think\Paginator): $i = 0; $__LIST__ = $right;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
    	    	<a href="<?php echo url('students/detail'); ?>?id=<?php echo $vo['id']; ?>"><img src="__ROOT__/uploads/<?php echo $vo['image']; ?>" width="280" height="180" /></a>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
</div>        <div id="rcontact">
	<div id="tels">
    	027-82809688    </div>
    <div id="address">
    	<span style="color:#FFF500;">光谷会馆：</span><br />        武汉市东湖高新区关山大道光谷天地九台别墅（光谷管）<br />        <span style="color:#FFF500;">后湖会馆：</span><br /><p>        武汉江岸区后湖百步亭万家汇三楼（后湖馆）</p>    </div>
</div>    
</div>
<!-- 教师学院右边共用 -->
<!-- 教师学院右边共用 -->

</div>

<!--底部开始-->
<div class="footer_zn">
			<div class="ft_1">
			<div class="wrap clear ">
				<ul>
					<li>联系我们</li> 
					<li class="rwm">
						<img src="__STATIC__/index/images/erweima.gif" />
					</li>
				</ul>
				
				<ul>
					<li>品牌介绍</li> 
					<li><a href="<?php echo url('index/index'); ?>">品牌荣誉</a></li>
					<li><a href="">联系方式</a></li>
				</ul>

				<ul>
					<li>精彩课程</li>
					<li><a href="">课程理论</a></li>
					<li><a href="">课程介绍</a></li>
					<li><a href="<?php echo url('coursest/index'); ?>">课程试听</a></li>
					<li><a href="">在线教学</a></li>
				</ul>
				<ul>
					<li>人才招聘</li>
					<li><a href="<?php echo url('rencai/index'); ?>">教师招聘</a></li>
					<li><a href="<?php echo url('rencai/index'); ?>">加盟招聘</a></li>
				</ul>
				<ul>
					<li>联系我们</li>
					<li><a href="<?php echo url('contact/index'); ?>">公司留言</a></li>
					<li><a href="<?php echo url('contact/index'); ?>">公司地图</a></li>

				</ul>
				<ul>
					<li class="rl">24小时全国服务热线</li>
					<li class="tel">400-920-3588</li>
					<li class="lx">
						<a class="qq" href="#"></a>
						<a class="wb" href="#"></a>
						<a class="wx" href="#"></a>
					</li>
				</ul>
				
			</div>
			</div>
			
			<div class="ft_2">
				<p>上海市黄浦区蒙自路223号209室 </p>
				<p>版权所有 Copyright © 2017 Otto2艺术美学 ALL RIGHT RESERVED 上海鄂图二世咨询服务有限公司</p>
			</div>
</div>
<!--底部结束-->

</body>
</html>