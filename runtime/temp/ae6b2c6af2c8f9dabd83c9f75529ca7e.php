<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:83:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\admin\admin_list.html";i:1502878856;s:80:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\header.html";i:1534562487;s:81:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\base_js.html";i:1533819104;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        Otto2美术教育
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="__STATIC__/admin/css/x-admin.css" media="all">
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
          UE.getEditor('content',{    //content为要编辑的textarea的id
          initialFrameWidth: 1100,   //初始化宽度
          initialFrameHeight: 500,   //初始化高度
  });
</script>
</head>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
              <a><cite>首页</cite></a>
              <a><cite>会员管理</cite></a>
              <a><cite>管理员列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"  href="javascript:location.replace(location.href);" title="刷新"><i class="layui-icon" style="line-height:30px">ဂ</i></a>
        </div>
        <div class="x-body">

            <table class="layui-table">
                <thead>
                    <tr>

                        <th>
                            ID
                        </th>
                        <th>
                            登录名
                        </th>

                        <th>
                            邮箱
                        </th>

                        <th>
                            登录次数
                        </th>
                        <th>
                            最后登录时间
                        </th>
                        <th>
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>

                        <td>
                            <?php echo $admin['id']; ?>
                        </td>
                        <td>
                            <?php echo $admin['username']; ?>
                        </td>

                        <td >
                            <?php echo $admin['email']; ?>
                        </td>

                        <td>
                            <?php echo $admin['login_count']; ?>
                        </td>

                        <td>
                            <?php echo $admin['last_time']; ?>
                        </td>

                        <td class="td-manage">

                            <a title="编辑" href="javascript:;" onclick="admin_edit('编辑','<?php echo url("admin/edit"); ?>'+'?id='+<?php echo $admin['id']; ?>,'4','','510')"
                               class="ml-5" style="text-decoration:none">
                                <i class="layui-icon">&#xe642;</i>
                            </a>

                        </td>
                    </tr>
                </tbody>
            </table>

            <div id="page"></div>
        </div>
        <script src="__STATIC__/admin/lib/layui/layui.js" charset="utf-8"></script>
<script src="__STATIC__/admin/js/x-admin.js"></script>
<script src="__STATIC__/admin/js/jquery.min.js"></script>
<script src="__STATIC__/admin/js/x-layui.js"></script>
<!--引入boostrap-->
<link rel="stylesheet" type="text/css" href="__STATIC__/admin/lib/bootstrap/css/bootstrap.css" />
<script type="text/javascript" src="__STATIC__/admin/lib/bootstrap/js/bootstrap.js"></script>
        <script>
            layui.use(['laydate','element','laypage','layer'], function(){
                $ = layui.jquery;//jquery
              laydate = layui.laydate;//日期插件
              lement = layui.element();//面包导航

              layer = layui.layer;//弹出层

              //以上模块根据需要引入

              laypage({
                cont: 'page'
                ,pages: 100
                ,first: 1
                ,last: 100
                ,prev: '<em><</em>'
                ,next: '<em>></em>'
              });

            });
       //编辑
            function admin_edit (title,url,id,w,h) {


                x_admin_show(title,url,w,h);

            }
            </script>

    </body>
</html>