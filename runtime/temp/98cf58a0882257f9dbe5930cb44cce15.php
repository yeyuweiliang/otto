<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:83:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\yuyue\yuyue_list.html";i:1534299748;s:80:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\header.html";i:1533890669;s:81:"D:\PHPTutorial\WWW\pen\otto2\public/../application/admin\view\public\base_js.html";i:1533819104;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        红杏科技
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="__STATIC__/admin/css/x-admin.css" media="all">
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
          UE.getEditor('content',{    //content为要编辑的textarea的id
          initialFrameWidth: 1100,   //初始化宽度
          initialFrameHeight: 500,   //初始化高度
  });
</script>
</head>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
              <a><cite>首页</cite></a>
              <a><cite>首页管理</cite></a>
              <a><cite>预约免费试听</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"  href="javascript:location.replace(location.href);" title="刷新"><i class="layui-icon" style="line-height:30px">ဂ</i></a>
        </div>
        <div class="x-body">
            <xblock><button class="layui-btn layui-btn-danger" onclick="delAll()"><i class="layui-icon">&#xe640;</i>批量删除</button><span class="x-right" style="line-height:40px">共有数据：<?php echo $count; ?>条</span></xblock>
            <table class="layui-table">
                <thead>
                    <tr>
                        <th>
                            <!--<input type="checkbox" name="" value="">-->
                        </th>
                        <th>
                            ID
                        </th>
                        <th>
                            姓名
                        </th>
                        <th>
                            家长姓名
                        </th>
                        <th>
                            电话
                        </th>
                        <th>
                            住址
                        </th>
                        <th>
                            个人介绍
                        </th>
                        <th>
                            预约时间
                        </th>
                        <th>
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody id="x-link">

                <?php if(is_array($message) || $message instanceof \think\Collection || $message instanceof \think\Paginator): $i = 0; $__LIST__ = $message;if( count($__LIST__)==0 ) : echo "暂时没有数据" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <tr>
                        <td>
                            <input type="checkbox" value="<?php echo $vo['id']; ?>" name="check[]">
                        </td>
                        <td>
                            <?php echo $vo['id']; ?>
                        </td>
                        <td>
                            <?php echo $vo['name']; ?>
                        </td>
                        <td>
                            <?php echo $vo['jzname']; ?>
                        </td>
                        <td>
                            <?php echo $vo['phone']; ?>
                        </td>
                        <td>
                            <?php echo $vo['address']; ?>
                        </td>
                        <td>
                            <?php echo $vo['content']; ?>
                        </td>
                        <td>
                            <?php echo date( 'Y-m-d H:i:s',$vo['time']); ?>
                        </td>
                        <td class="td-manage">
                            <a title="删除" href="javascript:;" onclick="message_del(this,'<?php echo $vo['id']; ?>')"
                            style="text-decoration:none">
                                <i class="layui-icon">&#xe640;</i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; endif; else: echo "暂时没有数据" ;endif; ?>
                </tbody>
            </table>

            <div style="text-align: center"><?php echo $message->render(); ?></div>
        </div>
        <script src="__STATIC__/admin/lib/layui/layui.js" charset="utf-8"></script>
<script src="__STATIC__/admin/js/x-admin.js"></script>
<script src="__STATIC__/admin/js/jquery.min.js"></script>
<script src="__STATIC__/admin/js/x-layui.js"></script>
<!--引入boostrap-->
<link rel="stylesheet" type="text/css" href="__STATIC__/admin/lib/bootstrap/css/bootstrap.css" />
<script type="text/javascript" src="__STATIC__/admin/lib/bootstrap/js/bootstrap.js"></script>
        <script>
            layui.use(['element','layer','form'], function(){
                $ = layui.jquery;//jquery
              lement = layui.element();//面包导航
              layer = layui.layer;//弹出层
              form = layui.form();

              //监听提交添加按钮
              form.on('submit(add)', function(data){
                console.log(data);
                //发异步，把数据提交给php
                  $.post("<?php echo url('create'); ?>",data.field,function (res) {
                      if (res.status == 1){
                          layer.alert(res.message, {icon: 6});
//                          $('#x-link').prepend('<tr><td><input type="checkbox"value="1"name=""></td><td>res.id</td><td>res.cate_order</td><td>res.cate_name</td><td class="td-manage"><a title="编辑"href="javascript:;"onclick="cate_edit(\'编辑\',\'cate-edit.html\',\'4\',\'\',\'510\')"class="ml-5"style="text-decoration:none"><i class="layui-icon">&#xe642;</i></a><a title="删除"href="javascript:;"onclick="message_del(this,\'1\')"style="text-decoration:none"><i class="layui-icon">&#xe640;</i></a></td></tr>');

                          setTimeout(function () {
                              window.location.replace(location.href);
                          },1000);


                      }else {
                          layer.alert(res.message, {icon: 6});
                      }
                  });
                return false;
              });


            })



              
            //批量删除提交
             function delAll () {
                layer.confirm('确认要删除吗？',function(index){
                    //捉到所有被选中的，发异步进行删除
                    layer.msg('删除成功', {icon: 1});
                });
             }
           
            /*-删除*/
            function message_del(obj,id){
                layer.confirm('确认要删除吗？',function(index){
                    //发异步删除数据
                    $.get("<?php echo url('delete'); ?>",{id:id});

                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                });
            }
            </script>

    </body>
</html>