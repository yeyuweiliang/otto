<?php

namespace app\admin\controller;
use app\admin\common\Base;
use think\Request;
use think\Db;
use app\admin\model\Yuyue as YuyueModel;

class Yuyue extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //分页数据
        $message = Db::table('yuyue')->paginate(5);
        // dump($message);exit;
        $count = Db::table('yuyue')->Count();
        //分配数据
        $this->assign('message',$message);
        $this->assign('count',$count);
        //渲染
        return $this->fetch('yuyue_list');

    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
        YuyueModel::destroy($id);

    }
}
