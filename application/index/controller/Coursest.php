<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Coursest extends Base
{
	//课程试听
	public function index(){
		//试听课程
		$coursest = Db::table('course_st')->paginate(8);
		//网站头部
		$header = Db::table('header')->select();
		//右边老师栏目
		$teacher = Db::table('teachers')->find();
		//推荐课程
		$tuijian = Db::table('tuijian')->select();

		$this->assign('header',$header);
		$this->assign('coursest',$coursest);
		$this->assign('teacher',$teacher);
		$this->assign('tuijian',$tuijian);

		return $this->fetch('coursest');
	}
}