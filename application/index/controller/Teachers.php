<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Teachers extends Base
{
	//师资力量
	public function index(){
		
		//网站头部
		$header = Db::table('header')->select();
		//培训老师列表
		$teachers = Db::table('teachers')->paginate(8);
		//推荐课程
		$tuijian = Db::table('tuijian')->select();
		//右边学员图片
        $right = Db::table('studentsxq')->where('id<4')->select();
		
		$this->assign('header',$header);
		$this->assign('teachers',$teachers);
		$this->assign('tuijian',$tuijian);
		$this->assign('right',$right);

		return $this->fetch('teachers');
	}

	//教师详情
	public function detail(){
		//获得数据
        $detail = Db::table('teachersxq')->where('id',input('id'))->find();
        //取出上一篇和下一篇的id
        $prev = Db::table('teachersxq')->where('id','<',input('id'))->order('id desc')->value('id');
        $next = Db::table('teachersxq')->where('id','>',input('id'))->order('id asc')->value('id');
        //老师名字
        $name_prev = Db::table('teachers')->where('id','<',input('id'))->order('id desc')->field('name')->find();
        $name_next = Db::table('teachers')->where('id','>',input('id'))->order('id asc')->field('name')->find();
        $name = Db::table('teachers')->where('id',input('id'))->field('name')->find();
        //推荐课程
		$tuijian = Db::table('tuijian')->select();
		//右边学员图片
        $right = Db::table('studentsxq')->where('id<4')->select();

        $this->assign('detail',$detail);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('name',$name);
        $this->assign('tuijian',$tuijian);
		$this->assign('right',$right);

        return $this->fetch('teacherslb');

	}
}