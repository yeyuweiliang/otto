<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Pinpai extends Base
{	
    //品牌介绍
    public function index(){
        //网站头部
        $header = Db::table('header')->select();
        //右边详情
        $right_info = Db::table('pinpai')->find();
        //名人推荐
        $mingren = Db::table('mingren')->select();
        //发展历程
        $licheng = Db::table('licheng')->find();
        //左边课程图片
        $left_info = Db::table('studentsxq')->where('id<4')->select();

        $this->assign('left_info',$left_info);
        $this->assign('header',$header);
        $this->assign('right_info',$right_info);
        $this->assign('mingren',$mingren);
        $this->assign('licheng',$licheng);

        return $this->fetch('pinpai');
    }
}