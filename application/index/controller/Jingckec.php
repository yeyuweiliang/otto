<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Jingckec extends Base	
{
	//精彩课程
	public function index(){
		//网站头部
		$header = Db::table('header')->select();
		//banner图
		$banner = Db::table('banner')->where('id',1)->find();
		//会馆分布
		$huiguang_1 = Db::table('huiguang')->where('id<5')->select();
		$huiguang_2 = Db::table('huiguang')->where('id>4')->select();
		//课程设置
		$course = Db::table('courseshow')->select();
		//获得一级表数据
    	$res = Db::table('category')->select();
        $cate = Db::table('cate')->where('pid',input('pid'))->select();
        // dump($cate);exit;
		//课程特色
		$tese = Db::table('teskec')->select();
		//课程特色右边照片
		$image= Db::table('teskec_img')->find();
		//教研团队
		$team = Db::table('team')->select();
		//教育成果
		$jiaoyu = Db::table('jiaoyu_img')->select();
		//儿童成长案例
		$anli = Db::table('anli_cz')->select();

		$this->assign('header',$header);
		$this->assign('banner',$banner);
		$this->assign('huiguang_1',$huiguang_1);
    	$this->assign('huiguang_2',$huiguang_2);
    	$this->assign('course',$course);
    	$this->assign('res',$res);
    	$this->assign('cate',$cate);
    	$this->assign('tese',$tese);
    	$this->assign('image',$image);
    	$this->assign('team',$team);
    	$this->assign('jiaoyu',$jiaoyu);
    	$this->assign('anli',$anli);

		return $this->fetch('course');
	}
	//课程展示详情
	public function detail(){
		//网站头部
		$header = Db::table('header')->select();
		//查询二级表的数据
        $data = Db::table('cate')->where('pid',input('pid'))->select();
        // dump($data);exit;
        //推荐课程
		$tuijian = Db::table('tuijian')->select();
		//右边学员图片
        $right = Db::table('studentsxq')->where('id<4')->select();

		$this->assign('header',$header);
		$this->assign('data',$data);
		$this->assign('tuijian',$tuijian);
		$this->assign('right',$right);

		return $this->fetch('courselb');
	}
}