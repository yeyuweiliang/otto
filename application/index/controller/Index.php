<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Index extends Base
{	
	//首页
    public function index()
    {	
    	
    	//轮播Banner图
    	$banner = Db::table('banner_sy')->select();
    	//两张banner图
    	$banner_one = Db::table('banner')->where('id',2)->find();
    	$banner_two = Db::table('banner')->where('id',3)->find();
    	//精彩课程
    	$course = Db::table('wonderful')->select();
    	//优秀作品
    	$works = Db::table('works')->select();
		//教师风采
		$teacher = Db::table('teacher')->select();
		//会馆分布
		$huiguang_1 = Db::table('huiguang')->where('id<5')->select();
		$huiguang_2 = Db::table('huiguang')->where('id>4')->select();
		//特色活动
		$huodong = Db::table('huodong')->select();
		//课程展示
		$kecshow = Db::table('kecshow')->select();
		//企业荣誉
		$honor = Db::table('honor')->select();
		//网站头部
		$header = Db::table('header')->select();
		//友情链接
		$link = Db::table('link')->select();
		
		
		
    	$this->assign('banner',$banner);
    	$this->assign('banner_one',$banner_one);
    	$this->assign('banner_two',$banner_two);
    	$this->assign('course',$course);
    	$this->assign('works',$works);
    	$this->assign('teacher',$teacher);
    	$this->assign('huiguang_1',$huiguang_1);
    	$this->assign('huiguang_2',$huiguang_2);
    	$this->assign('huodong',$huodong);
    	$this->assign('kecshow',$kecshow);
    	$this->assign('honor',$honor);
    	$this->assign('header',$header);
    	$this->assign('link',$link);
    	
        return $this->fetch();
    }
    //免费预约报名
    public function add(){
		//判断页面是否提交
		// print_r($_POST);exit();
		if(request()->isPost()){
			//接收传递过来的值
			//input都是封装好了，相当于POST['xxx'];
			$data = [
				'name' => input('name'),
				'jzname' => input('jzname'),
				'phone' => input('phone'),
				'address' => input('address'),
				'content' => input('content'),
				'time' => time()
			];
			// dump($data);exit;
			if(Db::table('yuyue')->insert($data)){  //添加数据
				return "<script>alert('预约成功');window.location.href='index';</script>";
			}else{
				return "<script>window.location.href='index';</script>";
			}
			return;
			
		}
		return $this->fetch('index');
	}
}
