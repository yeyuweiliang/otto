<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Rencai extends Base	
{
	//人才招聘
	public function index(){
		//网站头部
		$header = Db::table('header')->select();
		//人才列表
		$rencai = Db::table('rencai_zp')->paginate(8);
		//右边老师栏目
		$teacher = Db::table('teachers')->find();
		//推荐课程
		$tuijian = Db::table('tuijian')->select();

		$this->assign('header',$header);
		$this->assign('rencai',$rencai);
		$this->assign('teacher',$teacher);
		$this->assign('tuijian',$tuijian);

		return $this->fetch('rencai');
	}
	//招聘详情
	public function detail(){
		//获得数据
        $detail = Db::table('rencai_xq')->where('id',input('id'))->find();
        //网站头部
		$header = Db::table('header')->select();
		//右边老师栏目
		$teacher = Db::table('teachers')->find();
		//推荐课程
		$tuijian = Db::table('tuijian')->select();


        $this->assign('detail',$detail);
        $this->assign('header',$header);
        $this->assign('teacher',$teacher);
        $this->assign('tuijian',$tuijian);

        return $this->fetch('rencaixq');

	}
}