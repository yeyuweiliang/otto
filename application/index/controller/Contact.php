<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Contact extends Base
{
	//联系我们
    public function index(){
        //网站头部
        $header = Db::table('header')->select();
        //右边详情
        $right_info = Db::table('contact')->select();
        // dump($right_info);exit;
        //左边课程图片
        $left_info = Db::table('studentsxq')->where('id<4')->select();

        $this->assign('header',$header);
        $this->assign('right_info',$right_info);
        $this->assign('left_info',$left_info);

        return $this->fetch('contact');
    }
}