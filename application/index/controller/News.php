<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class News extends Base	
{
	//新闻中心
	public function index(){
	//网站头部
        $header = Db::table('header')->select();
	//左边课程图片
        $left_info = Db::table('studentsxq')->where('id<4')->select();
        //右边信息
        $right_info = Db::table('news')->paginate(6);

        $this->assign('left_info',$left_info);
		$this->assign('header',$header);
		$this->assign('right_info',$right_info);

		return $this->fetch('news');
	}
	//学员详情
	public function detail(){
        //点击次数
        $num = Db::table('newsxq')
            ->where('id', input('id'))
            ->update([
                'num' => ['exp','num+1'],
            ]);
        
	//获得数据
        $detail = Db::table('newsxq')->where('id',input('id'))->find();
        
        // $res = array($num);
        // dump($res);exit;
        //左边课程图片
        $left_info = Db::table('studentsxq')->where('id<4')->select();
        //取出上一篇和下一篇的id
        $prev = Db::table('newsxq')->where('id','<',input('id'))->order('id desc')->value('id');
        $next = Db::table('newsxq')->where('id','>',input('id'))->order('id asc')->value('id');
        //新闻标题
        $name_prev = Db::table('news')->where('id','<',input('id'))->order('id desc')->field('title')->find();
        $name_next = Db::table('news')->where('id','>',input('id'))->order('id asc')->field('title')->find();
        $name = Db::table('news')->where('id',input('id'))->field('title')->find();

        $this->assign('detail',$detail);
        $this->assign('num',$num);
        $this->assign('left_info',$left_info);
        $this->assign('prev',$prev);
        $this->assign('next',$next);
        $this->assign('name_prev',$name_prev);
        $this->assign('name_next',$name_next);
        $this->assign('name',$name);

        return $this->fetch('newsxq');

	}
}